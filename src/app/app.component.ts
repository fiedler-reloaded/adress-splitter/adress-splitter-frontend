import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ParseService} from './service/ParseService';
import {Address} from './dto/Address';
import {GenderEnum} from './dto/enums/GenderEnum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   * component, which manage all child components of the address splitter
   */

  constructor(public dialog: MatDialog,
              public parseService: ParseService) {}

  newTitle = '';
  unformattedAddress = '';
  language = '';

  resultExists = false;
  address = '';
  title: string[] = [];
  gender: GenderEnum = GenderEnum.not_specified;
  surName = '';
  name = '';

  addressTitles: string[] = [];

  letterAddress = '';

  error187 = false;
  error69 = false;
  error420 = false;
  noInputError = false;

  parseAddress(): void {
    /**
     * Parses unformatted address string in backend to get all address components.
     * Shows the parsed components in material cards.
     *
     */
    if (this.language === '') {
      this.noInputError = true;
      return;
    }
    this.parseService.parse(this.unformattedAddress).subscribe(
      (formattedAddress) => {

        this.resultExists = true;
        this.resetErrors();

        formattedAddress.error_list.forEach((errorNumber) => {
          if (errorNumber === 187) {
            this.error187 = true;
          } else if (errorNumber === 69 || errorNumber === 66) {
            this.error69 = true;
          } else if (errorNumber === 420) {
            this.error420 = true;
          }
        });

        console.log(formattedAddress);
        if (formattedAddress.address != null) {
          this.address = formattedAddress.address.address;

          this.title = formattedAddress.address.address_titles;
          // @ts-ignore
          this.gender = formattedAddress.address.gender as GenderEnum;
          this.surName = formattedAddress.address.sur_name;
          this.name = formattedAddress.address.name;
          this.getLetterAddress(formattedAddress.address);
        }
      }
    );
  }

  getLetterAddress(address: Address): void {
    /**
     * Generate letter address from all components and gender and language information.
     * Show letter address as text in material card.
     *
     * @param address - address object containing all address components for letter address
     */
    this.parseService.getLetterAddress(address, this.language).subscribe((letter) => {
      this.letterAddress = letter;
      console.log(this.letterAddress);
    });
  }

  onAddressChange(updatedValue: string, componentID: number): void {
    /**
     * update and manage letter address
     *
     * @param updatedValue - value to update
     * @param componendID - component id of single values of address to get right address attribute
     */
    if (componentID === 1) {
      this.address = updatedValue;
    } else if (componentID === 2) {
      this.title = updatedValue as unknown as string[];
      console.log(updatedValue);
    } else if (componentID === 3) {
      this.gender = updatedValue as unknown as GenderEnum;
    } else if (componentID === 4) {
      this.name = updatedValue;
    } else {
      this.surName = updatedValue;
    }
    const address: Address = {
      address_titles : this.title,
      name: this.name,
      sur_name: this.surName,
      gender: this.gender.toString(),
      address: this.address
    };
    this.getLetterAddress(address);
  }

  resetErrors(): void {
    /**
     * reset errors of this and child components
     */
    this.error187 = false;
    this.error69 = false;
    this.error420 = false;
    this.noInputError = false;
  }
}
