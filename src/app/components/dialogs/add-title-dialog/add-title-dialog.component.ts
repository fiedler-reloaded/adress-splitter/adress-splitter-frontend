import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {ParseService} from '../../../service/ParseService';

@Component({
  selector: 'app-add-title-dialog',
  templateUrl: './add-title-dialog.component.html',
  styleUrls: ['./add-title-dialog.component.css']
})
export class AddTitleDialogComponent {
  /**
   * Component, which represents a dialog to add new titles
   */

  constructor(
    public dialogRef: MatDialogRef<AddTitleDialogComponent>,
    private parseService: ParseService
  ) {}

  inputError = false;
  noInput = false;
  newTitle = '';

  onNoClick(): void {
    /**
     * closes dialog by clicks outside of the dialog component
     */
    this.dialogRef.close();
  }

  onCommit(): void {
    /**
     * adding string of variable newTitle
     */
    this.parseService.addAddressTitle(this.newTitle).subscribe((answer) => {
      if (answer) {
        this.dialogRef.close();
      } else {
        this.inputError = true;
      }
    }, error => {
      if (error.status === 404) {
        this.noInput = true;
      }
    });
  }

}
