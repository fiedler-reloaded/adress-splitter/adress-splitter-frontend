import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ParseService} from '../../service/ParseService';
import {MatDialog} from '@angular/material/dialog';
import {AddTitleDialogComponent} from '../dialogs/add-title-dialog/add-title-dialog.component';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  /**
   * Component, which represents the titles list on the right side.
   */

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onUpdateTitlesEvent = new EventEmitter<string[]>();

  addressTitles: string[] = [];

  constructor(private dialog: MatDialog, private parseService: ParseService) { }

  ngOnInit(): void {
    /**
     * Get all current titles from server when component is initialized.
     */
    this.getAddressTitles();
  }

  deleteTitle(title: string): void {
    /**
     * Delete the specified title in server.
     * The still existing titles are reloaded and the lists are refreshed.
     *
     * @param title - Address object to format
     */
    this.parseService.deleteAddressTitle(title).subscribe((result) => {
      if (result) {
        this.getAddressTitles();
      }
    });
  }

  openDialog(): void {
    /**
     * Creates the AddTitleDialogComponent as dialog and opens it, so the user can input a new title.
     *
     */
    const dialogRef = this.dialog.open(AddTitleDialogComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
      this.getAddressTitles();
    });
  }

  getAddressTitles(): void {
    /**
     * Get all current address titles from server and refresh all title lists.
     *
     */
    this.parseService.getAddressTitles().subscribe((titles) => {
      this.addressTitles = titles;
      this.onUpdateTitlesEvent.emit(this.addressTitles);
    });
  }

}
