import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Address} from '../dto/Address';
import {ParseResult} from '../dto/ParseResult';
import {LetterAddressInput} from '../dto/LetterAddressInput';
import {Injectable} from '@angular/core';

@Injectable()
export class ParseService {
  /**
   * Service, which communicate with backend
   */

  constructor(private httpClient: HttpClient) {
  }

  basePath = environment.basePath;

  parse(address: string): Observable<ParseResult> {
    /**
     * Use backend interface to parse address string to Address object
     *
     * @param address - address string to parse
     * @returns parsed address object with error codes from backend
     */
    return this.httpClient.post<ParseResult>(this.basePath + '/parse/' + address, null);
  }

  getLetterAddress(address: Address, language: string): Observable<string> {
    /**
     * Use backend interface to get formatted letter address of specified language
     *
     * @param address - Address object to format
     * @param language - language string for specific formatting
     * @returns formatted letter address as string
     */
    const languageCode = language === 'german' ? 0 : 1;
    const letterAddressInput: LetterAddressInput = new LetterAddressInput(address);
    letterAddressInput.address = address;
    return this.httpClient.post<string>(this.basePath + '/getLetterAddress/' + languageCode, letterAddressInput);
  }


  addAddressTitle(title: string): Observable<boolean> {
    /**
     * Use backend interface to add a title
     *
     * @param title - title string to add
     * @returns true if operation was successfully, otherwise false
     */
    return this.httpClient.post<boolean>(this.basePath + '/address-title/' + title, null);
  }

  getAddressTitles(): Observable<string[]> {
    /**
     * Use backend interface to get all titles
     *
     * @returns all titles as a string array
     */
    return this.httpClient.get<string[]>(this.basePath + '/address-title');
  }

  deleteAddressTitle(title: string): Observable<boolean> {
    /**
     * Use backend interface to delete a title
     *
     * @param title - title string to delete
     * @returns true if operation was successfully, otherwise false
     */
    return this.httpClient.delete<boolean>(this.basePath + '/address-title/' + title);
  }


}
