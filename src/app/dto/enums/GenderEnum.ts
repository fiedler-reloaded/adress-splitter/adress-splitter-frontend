// noinspection JSUnusedGlobalSymbols
export enum GenderEnum {
  /**
   * Enum of all genders for the letter address.
   */
  male,
  female,
  non_binary,
  not_specified
}
