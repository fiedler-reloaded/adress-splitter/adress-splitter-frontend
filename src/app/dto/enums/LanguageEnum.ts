// noinspection JSUnusedGlobalSymbols
export enum LanguageEnum {
  /**
   * Enum of all possible languages, in which the letter address in printed.
   */
  german,
  english,
  automatic
}
