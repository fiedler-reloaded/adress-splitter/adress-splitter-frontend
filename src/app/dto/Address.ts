export interface Address {
  /**
   * Parsed address object with all components.
   */
  address_titles: string[];
  address: string;
  sur_name: string;
  name: string;
  gender: string;
}
