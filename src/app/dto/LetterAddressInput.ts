import {Address} from './Address';

export class LetterAddressInput {
  /**
   * Input dto for calling get_letter_address interface of backend.
   */
  address: Address;
  constructor(address: Address) {
    this.address = address;
  }
}
