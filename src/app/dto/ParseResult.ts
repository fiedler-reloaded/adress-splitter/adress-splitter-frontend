import {Address} from './Address';

export interface ParseResult {
  /**
   * Result of parse interface in backend containing parsed address and list of occurred errors.
   */

  address: Address;
  error_list: number[];
}
